#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m"

#Check if the neccessary arguments are provided

if [[ ( $# != 5 )]]
then
    echo -e "${RED}Needed: first? cityDomain yubikeyPublicId yubikeySecret dockerId${NC}"
    exit
fi


find . -name "*.sh" -exec chmod +x {} \;

t=$5
find  ./deployment/kubernetes/deployment -type f -exec sed -i 's/iqop/'$t'/g' {} \;

if [[ "$1" == "true" ]] ; 
then
    echo -e "${GREEN}Generating Docker Images${NC}"
    cd deployment
    ./generateDockers.sh $1 $2 $3 $4 $5|| exit
    cd ..
fi
echo -e "${GREEN}Installing Virtual Machines, 12GB RAM and 8 CPU cores are the minimum${NC}"
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update
sudo apt install -y vagrant virtualbox sshpass
vagrant up || exit

echo -e "${GREEN}Deploying Kubernetes Pods${NC}"
vagrant ssh master -c "cd /vagrant/deployment/kubernetes/deployment/ && ./deploy.sh" || exit

echo -e "${GREEN} Generating Load Balancer${NC}"
vagrant ssh master -c "cd /vagrant/deployment/lb/ && ./runOnAllLB.sh"

echo -e "${GREEN}At this moment add to the clients /etc/hosts or to a DNS server the following entries${NC}"
ip=$( vagrant ssh master -c "ip addr show dev eth1 | grep 'inet ' | cut -d ' ' -f 6 | cut -d '/' -f 1" )
echo "manager.$2 $ip"
echo "endpoint.$2 $ip"