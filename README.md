# Inconnu Deployment 
**Description:** The complete automatized deployment

***
## Instructions
The following requirements are needed:

* x86 machine with virtualization enabled
* 8 cores
* 12GB RAM
* Docker Hub Account
* Yubikey with YubicoOTP support

First we need to update all the submodules
```bash
git clone --recurse-submodules https://Iqop20@bitbucket.org/Iqop20/inconnu.git
```


To check for the requirements run the following commands:
```bash
free -H   #To check the RAM 
nproc     #Check the number of CPU cores
```

Then install the yubikey personalization tool: ```yubikey-personalization-gui``` and configure one slot of the YubicoOTP, saving the aes secret and the public id



Then run the ```run.sh``` script

```bash
#Format
./run.sh <true|false build docker images?> <City Domain> <YubicoOTP public id> <YubicoOTP aes secret> <DockerHub ID>
```

The script will build 4 virtual machines using virtualbox and vagrant, installing kubernetes cluster on them, and configuring the ceph storage cluster, databases and load balancers. 

We, by default, deploy 3 openstack-swift instances on the virtual machines to simulate the off-premises storage. To change this behaviour the ```off-prem docker``` and the ```deployment/kubernetes/deployment/deploy.sh``` script must be changed


The kubernetes deployment is divided into 4 namespaces: **rook-ceph** for Ceph, **city-db** for mongoDB, **city-front** for Endpoint devices, City managers and teir dependencies, **off-prem** for off-premise controller and swift instances.

At the end of the process a set of DNS entries are returned. Those DNS entries must be applied to the DNS server or to the /etc/hosts files of the sub-managers and clients to be provisioned

**NOTE:** On the execution of ```run.sh``` the user is required to confirm that the kubernetes pods are sucessfully deployed. To verify that execute on a new terminal on this repository root folder the following: ```vagrant ssh master -c "watch kubectl get pods -A"```,this will provide information about the pods state, only press enter when all the pods required are ```Running```. Check ```screenshots folder``` to get deployment screenshots that might help you understand what to do on each step.

## Provisioning devices

To provision cli based devices instantiate an ptasc manager and append it to the city by doing appendToOtherManagerPool() using the city manager public keys stored at ```./deployment/CITY_PUBLIC```, and then append client devices into the manager.

The provisioning using android based devices uses the QR code located at ```./deployment/CITY_PUBLIC/CityQR.png```

## Scalling

By default the deployment user 3 nodes with one manager and 3 endpoint service clusters. However, the number of nodes and manager and endpoint service clusters can be easily scalled to cope with the demand on the resources thanks to kubernetes.

To scale the number of endpoint service clusters do the following:

```shell
vagrant ssh master -c "kubectl scale deployment endpoint -n city-db --replicas <NUMBER REPLICAS>"
```

To scale manager service cluster do the following:

```shell
vagrant ssh master -c "kubectl scale deployment manager -n city-db --replicas <NUMBER REPLICAS>"
```

***
To destroy the vms during the deployment execute ```vagrant destroy -f```. Keep in mind that this blocks the access to the kubernetes cluster and therefore all the implemented services.
***

For more information about implementation details check the dissertation